<?php

get_header();
$id = get_the_ID();
$expert = get_expert($id);
// $industry_expertises = maybe_unserialize($expert->industry_expertise);
$pic = get_field("profile_picture");
$position = get_field("position");
$email = get_field("email");
$phone = get_field("phone");
$linkedin = get_field("linkedin");
$description = get_field("description");
$valuetoclients = get_field("valuetoclients");
$experience = get_field("experience");
$other_things = get_field("other_things");
$location = get_field("location");
$industries = get_field("industry_expertise");
$name = explode(' ', trim(get_the_title()))[0];
?>


<section>
    <div class="container no-pad-gutters">
        <div class="back mb-4 mb-md-5">
            <i class="fa fa-caret-left align-bottom" style="font-size: 22px;" aria-hidden="true"></i> <a href="<?= site_url(); ?>" class="btn-outline-success text-uppercase px-0 ml-2">Back to team</a>
        </div>
        <!--May implement the expert's profile here -->
        <div class="row">
            <div class="col-md-4 text-right pr-5 mb-5">
                <img src="<?= $pic ?>" alt="<?= $name ?>" style="max-width:280px; border-radius:50%;">
            </div>
            <div class="col-md-8 pl-4">
                <h1><?php echo strtoupper(get_the_title()); ?></h1>

                <h3 class="pt-2"><?php echo strtoupper($position); ?></h3>

                <p class="pt-4" style="font-weight:500;"><i class="fa fa-map-marker fa-lg text-green pr-2" aria-hidden="true"></i><?= strtoupper($location->post_title) ?></p>

                <div class="row text-center pt-2">
                    <a href="mailto:<?= $email ?>" class="px-3 mx-1">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </a>
                    <a href="tel:<?= $phone ?>" class="px-3 mx-1">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                    </a>
                    <a href="<?= $linkedin ?>" class="px-3 mx-1">
                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                </div>

                <div class="mt-4"><?= $description ?></div>

                <h3 class="font-bold">Value to clients</h3>

                <div class="mt-2 customlist"><?= $valuetoclients ?></div>

                <h3 class="font-bold">Experience</h3>

                <div class="mt-2"><?= $experience ?></div>

                <h3 class="font-bold">Other things about <?= $name; ?></h3>

                <div class="mt-2 customlist"><?= $other_things ?></div>


            </div>
        </div>
    </div>
</section>

<?php
if (!empty($industries)) {
?>
<!--May implement the expert's industry expertise here -->
<section class="bg-dark-blue">
    <div class="container text-white no-pad-gutters">
        <h3 class="text-uppercase mb-4">Expertise</h3>
        <div class="row">
            <div class="col-md-12 mb-4">
                <div class="row">
                    <?php
                        foreach ($industries as $industry) {
                            // Access the relevant fields of the expert post
                            $name = $industry->post_title;
                            $icon = get_field("icon", $industry->ID);
                    ?>
                            <div class="col col-sm-2 col-lg-3 my-4 d-flex align-items-center">
                                <div class="col">
                                    <img src="<?= $icon; ?>" style="width:50px;" alt="">
                                </div>
                                <div class="col">
                                    <?= $name; ?>
                                </div>
                            </div>
                    <?php

                        }
                    ?>
                </div>
            </div>
        </div>
        <!--May implement the search and filter here-->
    </div>
</section>
<?php
}
get_footer();

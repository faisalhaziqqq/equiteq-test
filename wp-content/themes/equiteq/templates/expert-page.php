<?php
/* Template Name: Expert Page */
get_header();
$id = get_the_ID();
$page = get_post($id);
$industries = get_industries();
$locations = get_locations();
?>

<?php

/**Hero */
// hm_get_template_part('template-parts/hero', ['page' => $page]);

?>

<section class="bg-dark-blue" style="min-height:450px">
    <div class="container text-white no-pad-gutters mt-5">
        <h3 class="text-uppercase mb-4"><?php echo $page->title ?></h3>
        <div class="row">
            <div class="col-md-8 mb-4">
                <?php echo $page->post_content ?>
            </div>
        </div>
        <!--May implement the search and filter here-->
        <div class="row">
            <div class="col-8">

                <h4 class="text-uppercase mt-3 mb-2">Filters</h4>

                <div class="row">
                    <div class="sector pl-3 pr-3 " id="sector">
                        <span id="sector-text">Sector </span><i class="fa fa-caret-down ml-2" aria-hidden="true"></i>
                        <div class="sector-list d-none" id="sec-list">
                            <ul>
                                <li data-industry="All">All</li>
                                <?php
                                foreach ($industries as $industry) {
                                    echo "<li data-industry=" . $industry->post_name . ">" . $industry->post_title . "</li>";
                                }
                                ?>
                            </ul>
                        </div>
                    </div>

                    <div class="location" id="location">
                        <span class="ml-3" id="location-text">Location </span><i class="fa fa-caret-down ml-2" aria-hidden="true"></i>
                        <div class="location-list d-none" id="loc-list">
                            <ul>
                                <li data-value="All" data-title="All">All</li>
                                <?php
                                foreach ($locations as $location) {
                                    echo "<li data-value=" . $location->post_name . " data-title=" . $location->post_name . ">" . $location->post_title . "</li>";
                                }
                                ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-4">
                <h4 class="text-uppercase mt-3">Search</h4>
                <div class="input-group">
                    <div class="search-wrapper">
                        <input type="text" name="search-input" class="search-box" id="search-term">
                        <i class="fa fa-search icon-search">
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-white">
    <div class="container">
        <div class="row search-results" id="search-results">
        </div>
        <div class="row no-search-results" id="no-results">
        </div>
        <div class="row notsearch" id="notsearch-results">
            <?php
            $experts = get_experts();
            if (!empty($experts)) {
                foreach ($experts as $expert) {
                    // Access the relevant fields of the expert post
                    $name = $expert->post_title;
                    $profile = get_field('profile_picture', $expert->ID);
                    $position = get_field('position', $expert->ID);
                    $industries1 = get_field('industry_expertise', $expert->ID);
                    $location = get_field('location', $expert->ID);
                    $email = get_field('email', $expert->ID);
                    $phone = get_field('phone', $expert->ID);
                    $linkedin = get_field('linkedin', $expert->ID);
            ?>
                    <div class="col col-sm-1 col-md-3 mb-5 person-card <?= esc_html($location->post_name) ?>
                    <?php if (!empty($industries1)) {
                        foreach ($industries1 as $ind) {
                            $title = get_post_field('post_name', $ind->ID);
                            echo $title . " ";
                        }
                    }
                    ?>">

                        <div class="row justify-content-center mb-4 member_img">
                            <a href="<?= get_the_permalink($expert->ID) ?>"><img src="<?= $profile ?>" alt="<?= $name ?>" style="max-width:180px; border-radius:50%;"></a>
                        </div>

                        <div class="row justify-content-center mb-1 member_name">
                            <a href="<?= get_the_permalink($expert->ID) ?>" class="font-bold"><?= $name ?></a>
                        </div>

                        <div class="row text-center px-2 justify-content-center">
                            <?= $position ?>
                        </div>

                        <div class="row text-center px-2 my-1 justify-content-center font-italic">
                            <?= esc_html($location->post_title) ?>
                        </div>

                        <div class="row text-center justify-content-center">

                            <a href="mailto:<?= $email ?>" class="p-2 mx-2">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </a>

                            <a href="tel:<?= $phone ?>" class="p-2 mx-2">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </a>

                            <a href="<?= $linkedin ?>" class="p-2 mx-2">
                                <i class="fa fa-linkedin" aria-hidden="true"></i>
                            </a>

                        </div>
                    </div>
            <?php
                }
            }
            ?>
        </div>
    </div>
</section>
<?php
get_footer();
?>